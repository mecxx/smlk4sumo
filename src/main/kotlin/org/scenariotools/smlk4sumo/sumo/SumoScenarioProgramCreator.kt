package org.scenariotools.smlk4sumo.sumo

import org.eclipse.sumo.libtraci.StringVector
import org.scenariotools.smlk.ScenarioProgram
import org.scenariotools.smlk.scenario
import org.scenariotools.smlk4sumo.sumo.scenarios.*

fun createSumoScenarioProgram(sumoSimulation: SumoSimulation): ScenarioProgram {
    val scenarioProgram = ScenarioProgram("SumoLayer")
    scenarioProgram.activeAssumptionScenarios.add(
        simulationLifecycleController(sumoSimulation)
    )
    scenarioProgram.addEnvironmentMessageType(
        SumoSimulation::start,
        SumoSimulation::doStep,
        SumoSimulation::close,
    )
    scenarioProgram.activeGuaranteeScenarios.add(activeVehiclesController(sumoSimulation))
    scenarioProgram.activeGuaranteeScenarios.add(vehicleSpeedObserver(sumoSimulation))
    scenarioProgram.activeGuaranteeScenarios.add(vehicleSpeedIntObserver(sumoSimulation))
    scenarioProgram.activeGuaranteeScenarios.add(vehicleLaneObserver(sumoSimulation))
    scenarioProgram.activeGuaranteeScenarios.add(vehiclePositionObserver(sumoSimulation))
    scenarioProgram.activeGuaranteeScenarios.add(vehicleLanePositionObserver(sumoSimulation))
    scenarioProgram.activeGuaranteeScenarios.add(vehicleAccelerationObserver())
    scenarioProgram.activeGuaranteeScenarios.add(vehicleLaneChangingObserver())

    return scenarioProgram
}

private fun simulationLifecycleController(sumoSimulation: SumoSimulation) = scenario {
    request(sumoSimulation.start())
    do {
        request(sumoSimulation.doStep())
    } while (sumoSimulation.getMinExpectedNumber() > 0)
    request(sumoSimulation.close())
}

fun createSumoSimulation(
    sumo_bin: String = "sumo-gui.exe",
    config_file: String,
    step_length_sec: Double = 0.1,
    delay: Int = 50,
    start: Boolean = true,
    numberOfConnections: Int = 1,
    initFunctions: Set<(SumoSimulation) -> Unit> = setOf(),
): SumoSimulation {
    System.loadLibrary("libtracijni")
    val simulationStartParams =
        createStartParams(sumo_bin, config_file, step_length_sec, delay, start, numberOfConnections)
    val sumoSimulation = SumoSimulation(simulationStartParams)
    initFunctions.forEach { initFunction -> initFunction(sumoSimulation) }
    return sumoSimulation
}

fun createStartParams(
    sumoBin: String,
    configFile: String,
    stepLength: Double,
    delay: Int,
    start: Boolean,
    numberOfConnections: Int
): StringVector {
    val sv = StringVector()
    sv.add(sumoBin)
    sv.add("--configuration-file")
    sv.add(configFile)
    sv.add("--start")
    sv.add(start.toString())
    sv.add("--step-length")
    sv.add(stepLength.toString())
    sv.add("--delay")
    sv.add(delay.toString())
    sv.add("--num-clients")
    sv.add(numberOfConnections.toString())
    return sv
}

