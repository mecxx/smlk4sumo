package org.scenariotools.smlk4sumo.sumo

import mu.KotlinLogging
import org.eclipse.sumo.libtraci.Simulation
import org.eclipse.sumo.libtraci.StringVector
import org.scenariotools.smlk.ObjectEvent
import org.scenariotools.smlk.event
import java.io.IOException
import java.util.concurrent.ConcurrentHashMap
import kotlin.math.roundToInt


private val logger = KotlinLogging.logger {}

abstract class SumoSimulationElement(val id: String, val sumoSimulation: SumoSimulation) {
    override fun toString() = "${this::class.simpleName}_$id"
}

class SumoSimulation(sumoTraciSimulationParams: StringVector) {

    init {
        try {
            Simulation.start(sumoTraciSimulationParams)
            Simulation.step() // need to perform a time step before the simulation elements can be retrieved.
        } catch (exception: IOException) {
            if (exception.message!!.contains("Cannot run program \"sumo-gui.exe\"")) {
                println("Cannot find \"sumo-gui.exe\" -- did you include it in the system PATH variable? Alternatively change path in Main.kt. Also make sure to set SUMO_HOME")
            }
            println(exception)

        }
    }

    var closed = false

    fun start() = event {}

    fun close() = event {
        SumoProxy.close()
        closed = true
    }

    var currentTimeStep = 0

    fun doStep() = event {
        SumoProxy.doStep()
        currentTimeStep = SumoProxy.getCurrentTime()
        currentTimeStep
    }

    fun getMinExpectedNumber() = SumoProxy.getMinExpectedNumber()

    private val vehicles: MutableMap<String, SumoVehicle> = ConcurrentHashMap()
    val activeVehicles: MutableMap<String, SumoVehicle> = ConcurrentHashMap()

    fun getArrivedIDList() = SumoProxy.getArrivedIDList()
    fun getDepartedIDList() = SumoProxy.getDepartedIDList()

    fun createVehicle(
        id: String,
        routeId: String,
        type: SumoVehicleType = SumoVehicleType.DEFAULT,
        departDelay: String = "0",
        departSpeed: Double = 0.0,
        maxSpeed: Double? = null,
        laneChangingMode: SumoLaneChangingMode = SumoLaneChangingMode.DEFAULT
    ): SumoVehicle {
        val newVehicle = SumoVehicle(this, id, routeId, type, departDelay, departSpeed)
        if (maxSpeed != null) {
            newVehicle.setMaxSpeedDirectly(maxSpeed)
        }
        newVehicle.setLaneChangingMode(laneChangingMode)
        vehicles[id] = newVehicle
        return newVehicle
    }

    fun vehicleDeparted(id: String) = event(id) {
        val newVehicle = vehicles[id]
        if (newVehicle != null) {
            activeVehicles[id] = newVehicle
        }
    }

    fun vehicleDisAppeared(id: String) = event(id) {
        activeVehicles.remove(id)
        vehicles.remove(id)
    }

    fun vehicleListUpdated() = event {}

    override fun toString() = "SumoSimulation"
}

class SumoVehicle(
    sumoSimulation: SumoSimulation,
    id: String,
    routeId: String,
    type: SumoVehicleType,
    departDelay: String,
    departSpeed: Double
) :
    SumoSimulationElement(id, sumoSimulation) {



    init {
        SumoProxy.disableSpeedFactorDeviation(type.typeId)
        SumoProxy.addVehicle(
            id,
            routeId,
            type.typeId,
            departDelay,
            SumoDepartLane.FIRST.value,
            SumoDepartPosition.BASE.value,
            departSpeed.toString()
        )
    }

    fun getPosition() = event {
        SumoProxy.getPosition(id)
    }

    fun getLanePosition() = event {
        SumoProxy.getVehicleLanePosition(id)
    }

    fun getSpeed() = event {
        SumoProxy.getSpeed(id)
    }

    fun getSpeedInt() = event {
        SumoProxy.getSpeed(id).roundToInt()
    }

    fun getLane() = event {
        SumoProxy.getLane(id)
    }

    fun changedLane() = event {

    }

    fun laneDistance(distance: Double, toVehicle: SumoVehicle) = event(distance, toVehicle) {

    }

    fun increasedSpeed(oldSpeed: Double, newSpeed: Double) = event(oldSpeed, newSpeed) {

    }

    fun decreasedSpeed(oldSpeed: Double, newSpeed: Double) = event(oldSpeed, newSpeed) {

    }

    fun slowDown(speed: Double = 0.0, duration: Double = 10.0) = event(speed, duration) {
        SumoProxy.slowDown(id, speed, duration)
    }

    fun setSpeed(speed: Double) = event(speed) {
        SumoProxy.setSpeed(id, speed)
    }

    fun stop() = event {
        SumoProxy.stop(id)
    }

    fun remove() = event {
        SumoProxy.remove(id)
    }

    fun setMaxSpeed(maxSpeed: Double) = event(maxSpeed) {
        SumoProxy.setMaxSpeed(id, maxSpeed)
    }

    fun setMaxSpeedDirectly(maxSpeed: Double) {
        SumoProxy.setMaxSpeed(id, maxSpeed)
    }

    fun setLaneChangingMode(laneChangingMode: SumoLaneChangingMode) {
        SumoProxy.setLaneChangingMode(id, laneChangingMode.value)
    }

    fun changeLane(laneIndex: Int): ObjectEvent<SumoVehicle, Unit> {
        return event(laneIndex) {
            SumoProxy.changeLane(id, laneIndex, 5.0)
        }
    }

    fun setColor(color: SumoColor) {
        SumoProxy.setColor(id, color)
    }

    fun reachedTargetSpeed(speed: Double) = event(speed) {
    }
}

enum class SumoDepartLane(val value: String) {
    RANDOM("random"), FREE("free"), ALLOWED("allowed"), BEST("best"), FIRST("first")
}

enum class SumoDepartPosition(val value: String) {
    RANDOM("random"), FREE("free"), BASE("base"), LAST("last"), RANDOM_FREE("random_free")
}

enum class SumoLaneChangingMode(val value: Int) {
    DEFAULT(1621), NO_CHANGES(Integer.parseInt("011000000100", 2))
}

enum class SumoVehicleType(val typeId: String) {
    DEFAULT("DEFAULT_VEHTYPE")
}

enum class SumoColor(val r: Int, val g: Int, val b: Int) {
    RED(255, 0, 0), GREEN(0, 255, 0), BLUE(0, 0, 255),
}
