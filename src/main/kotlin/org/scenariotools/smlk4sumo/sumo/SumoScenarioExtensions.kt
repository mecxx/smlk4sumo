package org.scenariotools.smlk4sumo.sumo

import org.scenariotools.smlk.*

val timeStepEvent = SumoSimulation::doStep.symbolicEvent()

suspend fun <R:Any,T> Scenario.urgent(objectEvent: ObjectEvent<R,T>, additionalEventsForbiddenAtThisSyncPoint : IEventSet = NOEVENTS) : ObjectEvent<R,T>
        = request(objectEvent, MutableNonConcreteEventSet(timeStepEvent, additionalEventsForbiddenAtThisSyncPoint))


suspend fun Scenario.waitForSimulationSteps(numberOfSteps : Int){
    for (i in 0 .. numberOfSteps) {
        waitFor(timeStepEvent)
    }
}

suspend inline fun Scenario.expect(condition : ()->Boolean, afterNumberOfSteps: Int, message:String = ""){
    var counter = 0
    while (counter < afterNumberOfSteps){
        if (condition()) return
        waitFor(SumoSimulation::doStep.symbolicEvent())
        counter++
    }
    violation(message)
}
