package org.scenariotools.smlk4sumo.sumo

import org.eclipse.sumo.libtraci.*

object SumoProxy {

    @Synchronized
    fun close() = Simulation.close()

    @Synchronized
    fun doStep() = Simulation.step()

    @Synchronized
    fun getCurrentTime() = Simulation.getCurrentTime()

    @Synchronized
    fun addVehicle(
        id: String,
        route: String,
        typeID: String,
        departDelay: String,
        departLane: String,
        departPosition: String,
        departSpeed: String
    ) =
        Vehicle.add(id, route, typeID, departDelay, departLane, departPosition, departSpeed)

    @Synchronized
    fun getVehicleType(id: String) = Vehicle.getTypeID(id)

    @Synchronized
    fun getVehicleLane(id: String) = Vehicle.getLaneID(id)

    @Synchronized
    fun getVehicleLanePosition(id: String) = Vehicle.getLanePosition(id)

    @Synchronized
    fun getPosition(id: String) = Vehicle.getPosition(id)

    @Synchronized
    fun getSpeed(id: String) = Vehicle.getSpeed(id)

    @Synchronized
    fun getLane(id: String) = Vehicle.getLaneID(id)

    @Synchronized
    fun slowDown(id: String, speed: Double, duration: Double) = Vehicle.slowDown(id, speed, duration)

    @Synchronized
    fun setSpeed(id: String, speed: Double) = Vehicle.setSpeed(id, speed)

    @Synchronized
    fun stop(id: String, edgeId: String, pos: Double, laneIndex: Int, duration: Double) =
        Vehicle.setStop(id, edgeId, pos, laneIndex, duration)

    @Synchronized
    fun stop(id: String) = Vehicle.setSpeed(id, 0.0)

    @Synchronized
    fun setMaxSpeed(id: String, speed: Double) = Vehicle.setMaxSpeed(id, speed)

    @Synchronized
    fun setLaneChangingMode(id: String, laneChangingMode: Int) = Vehicle.setLaneChangeMode(id, laneChangingMode)

    @Synchronized
    fun changeLane(id: String, laneIndex: Int, duration: Double) = Vehicle.changeLane(id, laneIndex, duration)

    @Synchronized
    fun remove(id: String) = Vehicle.remove(id)

    @Synchronized
    fun exists(id: String) = Vehicle.getIDList().contains(id)

    @Synchronized
    fun getDepartedIDList() = Simulation.getDepartedIDList()!!

    @Synchronized
    fun getArrivedIDList() = Simulation.getArrivedIDList()!!

    @Synchronized
    fun getMinExpectedNumber() = Simulation.getMinExpectedNumber()

    @Synchronized
    fun disableSpeedFactorDeviation(typeID: String) = VehicleType.setSpeedDeviation(typeID, 0.0)

    @Synchronized
    fun getEdgeIDs() = Edge.getIDList()

    @Synchronized
    fun setColor(id: String, color: SumoColor) = Vehicle.setColor(id, TraCIColor(color.r, color.g, color.b))

    @Synchronized
    fun getLastStepPersonIDs(id: String) = Edge.getLastStepPersonIDs(id)
}
