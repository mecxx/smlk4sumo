package org.scenariotools.smlk4sumo.sumo.scenarios

import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.symbolicEvent
import org.scenariotools.smlk4sumo.sumo.SumoSimulation
import org.scenariotools.smlk4sumo.sumo.SumoVehicle

fun activeVehiclesController(sumoSimulation: SumoSimulation) = scenario(sumoSimulation.doStep()) {
    for (id in sumoSimulation.getArrivedIDList()) {
        request(sumoSimulation.vehicleDisAppeared(id))
    }
    for (id in sumoSimulation.getDepartedIDList()) {
        request(sumoSimulation.vehicleDeparted(id))
    }
    request(sumoSimulation.vehicleListUpdated())
}

fun vehiclesPositionAndSpeedObserver(sumoSimulation: SumoSimulation) = scenario(sumoSimulation.doStep()) {
    for (sumoVehicle in sumoSimulation.activeVehicles.values) {
        request(sumoVehicle.getLane())
        request(sumoVehicle.getPosition())
        request(sumoVehicle.getLanePosition())
        request(sumoVehicle.getSpeed())
        request(sumoVehicle.getSpeedInt())
    }
}

fun vehicleSpeedObserver(sumoSimulation: SumoSimulation) = scenario(sumoSimulation.doStep()) {
    for (sumoVehicle in sumoSimulation.activeVehicles.values) {
        request(sumoVehicle.getSpeed())
    }
}

fun vehicleSpeedIntObserver(sumoSimulation: SumoSimulation) = scenario(sumoSimulation.doStep()) {
    for (sumoVehicle in sumoSimulation.activeVehicles.values) {
        request(sumoVehicle.getSpeedInt())
    }
}

fun vehicleLaneObserver(sumoSimulation: SumoSimulation) = scenario(sumoSimulation.doStep()) {
    for (sumoVehicle in sumoSimulation.activeVehicles.values) {
        request(sumoVehicle.getLane())
    }
}

fun vehicleLanePositionObserver(sumoSimulation: SumoSimulation) = scenario(sumoSimulation.doStep()) {
    for (sumoVehicle in sumoSimulation.activeVehicles.values) {
        request(sumoVehicle.getLanePosition())
    }
}

fun vehiclePositionObserver(sumoSimulation: SumoSimulation) = scenario(sumoSimulation.doStep()) {
    for (sumoVehicle in sumoSimulation.activeVehicles.values) {
        request(sumoVehicle.getPosition())
    }
}


fun vehicleAccelerationObserver() =
    scenario(SumoVehicle::getSpeed.symbolicEvent()) {
        val sumoVehicle = it.receiver
        val currentSpeed = it.result()
        if (currentSpeed != null) {
            val nextSpeedEvent = waitFor(sumoVehicle.getSpeed())
            val newSpeed = nextSpeedEvent.result()
            if (newSpeed != null) when {
                currentSpeed < newSpeed -> request(sumoVehicle.increasedSpeed(currentSpeed, newSpeed))
                currentSpeed > newSpeed -> request(sumoVehicle.decreasedSpeed(currentSpeed, newSpeed))
            }
        }
    }

fun vehicleAccelerationObserver(sumoVehicle: SumoVehicle) =
    scenario(sumoVehicle.getSpeed()) {
        val currentSpeed = it.result()
        if (currentSpeed != null) {
            val nextSpeedEvent = waitFor(sumoVehicle.getSpeed())
            val newSpeed = nextSpeedEvent.result()
            if (newSpeed != null) when {
                currentSpeed < newSpeed ->
                    request(sumoVehicle.increasedSpeed(
                        currentSpeed,
                        newSpeed)
                    )
                currentSpeed > newSpeed ->
                    request(sumoVehicle.decreasedSpeed(
                        currentSpeed,
                        newSpeed)
                    )
            }
        }
    }


fun vehicleLaneChangingObserver() =
    scenario(SumoVehicle::getLane.symbolicEvent()) {
        val sumoVehicle = it.receiver
        val currentLane = it.result()
        if (currentLane != null) {
            val waitFor = waitFor(sumoVehicle.getLane())
            if (waitFor.result() != null) {
                if (currentLane != waitFor.result()) {
                    request(sumoVehicle.changedLane())
                }
            }
        }
    }

