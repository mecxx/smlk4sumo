package slow_down_car.scenarios

import org.scenariotools.smlk.param
import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.symbolicEvent
import org.scenariotools.smlk4sumo.sumo.SumoVehicle

fun slowDownAtTargetSpeed(vehicle: SumoVehicle, times: Int, topSpeed: Double, lowSpeed: Double) = scenario()
{
    repeat(times) {
        waitFor(vehicle::reachedTargetSpeed.symbolicEvent() param (listOf(topSpeed)))
        request(vehicle.slowDown(lowSpeed))
        waitFor(vehicle::reachedTargetSpeed.symbolicEvent() param (listOf(lowSpeed)))
    }
}

fun targetSpeedExceededController(vehicle: SumoVehicle, targetSpeed: Double) = scenario(vehicle.getSpeed())
{
    val speed = it.result()
    if (speed != null && speed >= targetSpeed) {
        request(vehicle.reachedTargetSpeed(targetSpeed))
    }
}

fun fallenBelowTargetSpeedController(vehicle: SumoVehicle, targetSpeed: Double) = scenario(vehicle.getSpeed())
{
    val speed = it.result()
    if (speed != null && speed <= targetSpeed) {
        request(vehicle.reachedTargetSpeed(targetSpeed))
    }
}

