package slow_down_car

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.scenariotools.smlk.ScenarioProgram
import org.scenariotools.smlk.runTest
import org.scenariotools.smlk.symbolicEvent
import org.scenariotools.smlk4sumo.sumo.SumoVehicle
import org.scenariotools.smlk4sumo.sumo.createSumoScenarioProgram
import org.scenariotools.smlk4sumo.sumo.createSumoSimulation
import slow_down_car.scenarios.fallenBelowTargetSpeedController
import slow_down_car.scenarios.slowDownAtTargetSpeed
import slow_down_car.scenarios.targetSpeedExceededController

fun main() {
    val sumoScenarioProgram = createScenarioProgram()
    runBlocking {
        sumoScenarioProgram.run()
    }
}

fun createScenarioProgram(): ScenarioProgram {

    val sumoSimulation = createSumoSimulation(
        config_file = "src/test/resources/simple_car/run.sumocfg",
    )
    val sumoScenarioProgram = createSumoScenarioProgram(sumoSimulation)

    val vehicleV1 = sumoSimulation.createVehicle("v1", "route_0")
    val vehicleV2 = sumoSimulation.createVehicle("v2", "route_0")
    val vehicleV3 = sumoSimulation.createVehicle("v3", "route_0")

    val scenarios = listOf(
        slowDownAtTargetSpeed(vehicleV1, 2, 10.0, 0.5),
        targetSpeedExceededController(vehicleV1, 10.0),
        fallenBelowTargetSpeedController(vehicleV1, 0.5),
    )

    sumoScenarioProgram.activeGuaranteeScenarios.addAll(scenarios)

    return sumoScenarioProgram
}

class MyScenarioTest {
    private val scenarioProgram = createScenarioProgram()
    @Test
    fun testSlowDown() = runTest(scenarioProgram) {
        mustOccur(SumoVehicle::slowDown.symbolicEvent())
    }
}



