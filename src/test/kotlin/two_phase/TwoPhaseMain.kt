package two_phase

import kotlinx.coroutines.runBlocking
import two_phase.scenarios.createScenarioProgram

val configBasedir = "src/test/resources/simple_car/"

fun main() {
    val scenarioProgram = createScenarioProgram("run.sumocfg", routeId = "route_2")
    runBlocking {
        scenarioProgram.run()
    }
}




