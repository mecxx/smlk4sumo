package two_phase.scenarios

import org.scenariotools.smlk.Scenario
import org.scenariotools.smlk.scenario
import org.scenariotools.smlk4sumo.sumo.SumoVehicle

fun acceleratesToDestinationSpeed(
    drivingVehicle: SumoVehicle,
    destinationSpeed: Double,
    followingScenario: suspend Scenario.() -> Unit
) =
    scenario(drivingVehicle.getSpeed())
    {
        val speed = it.result()
        if (speed != null && speed >= destinationSpeed) {
            launchScenario(followingScenario)
        }
    }

fun driveWithConstantSpeed(drivingVehicle: SumoVehicle, targetSpeed: Double) =
    scenario()
    {
        request(drivingVehicle.setMaxSpeed(targetSpeed))
    }
