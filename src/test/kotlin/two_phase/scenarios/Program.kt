package two_phase.scenarios

import org.scenariotools.smlk.ScenarioProgram
import org.scenariotools.smlk4sumo.sumo.SumoColor
import org.scenariotools.smlk4sumo.sumo.createSumoScenarioProgram
import org.scenariotools.smlk4sumo.sumo.createSumoSimulation
import two_phase.configBasedir

fun createScenarioProgram(map: String, routeId: String = "route_1", color: SumoColor = SumoColor.GREEN): ScenarioProgram {
    val sumoSimulation = createSumoSimulation(
        config_file = configBasedir + map
    )
    val sumoScenarioProgram = createSumoScenarioProgram(sumoSimulation)
    val drivingVehicle = sumoSimulation.createVehicle("v1", routeId)
    drivingVehicle.setColor(color)
    sumoScenarioProgram.activeGuaranteeScenarios.add(
        acceleratesToDestinationSpeed(drivingVehicle, 10.0, driveWithConstantSpeed(drivingVehicle, 12.0))
    )
    return sumoScenarioProgram
}
