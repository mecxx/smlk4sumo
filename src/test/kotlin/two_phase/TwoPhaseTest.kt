package two_phase

import org.junit.jupiter.api.Test
import org.scenariotools.smlk.runTest
import org.scenariotools.smlk.symbolicEvent
import org.scenariotools.smlk4sumo.sumo.SumoSimulation
import slow_down_car.createScenarioProgram

class TwoPhaseTest {

    private val scenarioProgram = createScenarioProgram()

    @Test
    fun testHotCold() = runTest(scenarioProgram, timeoutMillis = 30000) {
        repeat(3) {
            mustOccur(SumoSimulation::doStep.symbolicEvent())
        }
    }

}
