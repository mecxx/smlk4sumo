package cut_in_and_slow

import org.scenariotools.smlk.Scenario
import org.scenariotools.smlk.param
import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.symbolicEvent
import org.scenariotools.smlk4sumo.sumo.SumoVehicle


fun vehicleLaneDistanceObserver(fromVehicle: SumoVehicle, toVehicle: SumoVehicle) =
    scenario(fromVehicle.getLanePosition()) {
        val lanePositionFromVehicle = it.result()
        val waitFor = waitFor(toVehicle.getLanePosition())
        val lanePositionToVehicle = waitFor.result()
        if (lanePositionFromVehicle != null && lanePositionToVehicle != null) {
            val distance = lanePositionFromVehicle - lanePositionToVehicle
            request(fromVehicle.laneDistance(distance, toVehicle))
        }
    }

fun changeLaneAtCarDistance(
    overtakingVehicle: SumoVehicle,
    overtakenVehicle: SumoVehicle,
    overtakingDistance: Double,
    toLaneIndex: Int
) =
    scenario() {
        changeLaneScenarioBody(overtakingVehicle, overtakenVehicle, overtakingDistance, toLaneIndex)
    }


fun cutInAtCarDistance(
    cuttingInVehicle: SumoVehicle,
    overtakenVehicle: SumoVehicle,
    cutInDistance: Double,
    fromLaneIndex: Int,
    toLaneIndex: Int
) =
    scenario(cuttingInVehicle::changeLane.symbolicEvent() param (listOf(fromLaneIndex))) {
        changeLaneScenarioBody(cuttingInVehicle, overtakenVehicle, cutInDistance, toLaneIndex)
    }

private suspend fun Scenario.changeLaneScenarioBody(
    laneChangingVehicle: SumoVehicle,
    referenceVehicle: SumoVehicle,
    cutInDistance: Double,
    toLaneIndex: Int
) {
    while (true) {
        val laneDistanceEvent =
            waitFor(laneChangingVehicle::laneDistance.symbolicEvent() param (Pair("toVehicle", referenceVehicle)))
        val distance = laneDistanceEvent.parameters[0] as Double
        if (distance > cutInDistance) {
            request(laneChangingVehicle.changeLane(toLaneIndex))
            break
        }
    }
}


fun setSpeedWhenChangingToLane(vehicle: SumoVehicle, lineIndex: Int, targetSpeed: Double) =
    scenario(vehicle::changeLane.symbolicEvent() param (listOf(lineIndex))) {
        request(vehicle.slowDown())
        request(vehicle.setSpeed(targetSpeed))
    }
