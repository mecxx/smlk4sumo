package cut_in_and_slow

import kotlinx.coroutines.runBlocking
import org.scenariotools.smlk.ScenarioProgram
import org.scenariotools.smlk4sumo.sumo.SumoLaneChangingMode
import org.scenariotools.smlk4sumo.sumo.createSumoScenarioProgram
import org.scenariotools.smlk4sumo.sumo.createSumoSimulation

fun main() {
    val scenarioProgram = createCutInAndSlowScenarioProgram()
//    main2()
    runBlocking {
        scenarioProgram.run()
    }
}

fun createCutInAndSlowScenarioProgram(): ScenarioProgram {

    val sumoSimulation = createSumoSimulation(
        config_file = "src/test/resources/simple_car/run.sumocfg",
    )
    val sumoScenarioProgram = createSumoScenarioProgram(sumoSimulation)

    val overtankenVehicle = sumoSimulation.createVehicle(
        id = "v1",
        routeId = "route_0",
        maxSpeed = 6.0,
        laneChangingMode = SumoLaneChangingMode.NO_CHANGES
    )
    val overtakingVehicle = sumoSimulation.createVehicle(
        id = "v2",
        routeId = "route_0",
        departDelay = "10",
        laneChangingMode = SumoLaneChangingMode.NO_CHANGES
    )

    val scenarios = listOf(
        vehicleLaneDistanceObserver(overtakingVehicle, overtankenVehicle),
        changeLaneAtCarDistance(overtakingVehicle, overtankenVehicle, -15.0, 1),
        cutInAtCarDistance(overtakingVehicle, overtankenVehicle, 10.0, 1, 0),
        setSpeedWhenChangingToLane(overtakingVehicle, 0, 5.0)

    )

    sumoScenarioProgram.activeGuaranteeScenarios.addAll(scenarios)

    return sumoScenarioProgram
}

fun createCutInAndSlowScenarioProgram2(): ScenarioProgram {

    val sumoSimulation = createSumoSimulation(
        config_file = "src/test/resources/simple_car/run.sumocfg",
    )
    val sumoScenarioProgram = createSumoScenarioProgram(sumoSimulation)

    val overtankenVehicle = sumoSimulation.createVehicle("v1", "route_2")
    overtankenVehicle.setMaxSpeedDirectly(8.5)
    overtankenVehicle.setLaneChangingMode(SumoLaneChangingMode.NO_CHANGES)
    val overtakingVehicle = sumoSimulation.createVehicle(id = "v2", routeId = "route_2", departDelay = "5")
    overtakingVehicle.setLaneChangingMode(SumoLaneChangingMode.NO_CHANGES)

    sumoScenarioProgram.activeGuaranteeScenarios.add(
        changeLaneAtCarDistance(
            overtakingVehicle,
            overtankenVehicle,
            20.0,
            0
        )
    )
    sumoScenarioProgram.activeGuaranteeScenarios.add(
        cutInAtCarDistance(
            overtakingVehicle,
            overtankenVehicle,
            10.0,
            1,
            0
        )
    )
    sumoScenarioProgram.activeGuaranteeScenarios.add(setSpeedWhenChangingToLane(overtakingVehicle, 0, 5.0))
    return sumoScenarioProgram
}
