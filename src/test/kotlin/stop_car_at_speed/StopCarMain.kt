package stop_car_at_speed

import kotlinx.coroutines.runBlocking
import org.scenariotools.smlk4sumo.sumo.createSumoScenarioProgram
import org.scenariotools.smlk4sumo.sumo.createSumoSimulation
import stop_car_at_speed.scenarios.removeVehicleAfterStop
import stop_car_at_speed.scenarios.stopAtSpeed13

fun main() {

    val sumoSimulation = createSumoSimulation(
        config_file = "src/test/resources/simple_car/run.sumocfg"
    )
    val sumoScenarioProgram = createSumoScenarioProgram(sumoSimulation)

    val sumoVehicle = sumoSimulation.createVehicle("v1", "route_0")
    sumoScenarioProgram.activeGuaranteeScenarios.add(stopAtSpeed13(sumoVehicle))
    sumoScenarioProgram.activeGuaranteeScenarios.add(removeVehicleAfterStop(sumoVehicle))

    runBlocking {
        sumoScenarioProgram.run()
    }
}
