package stop_car_at_speed.scenarios

import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.symbolicEvent
import org.scenariotools.smlk4sumo.sumo.SumoVehicle
import org.scenariotools.smlk4sumo.sumo.urgent

fun stopAtSpeed13(sumoVehicle:SumoVehicle) = scenario(sumoVehicle.getSpeed())
{
    val speed = it.result()
    println("Current speed of $sumoVehicle is $speed")
    if (speed != null && speed >= 13) {
        println("Stop car $sumoVehicle")
        urgent(sumoVehicle.stop())
    }
}

fun removeVehicleAfterStop(sumoVehicle:SumoVehicle) = scenario(sumoVehicle::stop.symbolicEvent())
{
    while (true) {
        val currentSpeed = waitFor(sumoVehicle.getSpeed()).result()
        if (currentSpeed == 0.0) {
            println("Remove vehicle $sumoVehicle because its speed is 0.0")
            request(sumoVehicle.remove())
            break
        }
    }
}
