smlk4sumo shows the combination of smlk and sumo to model and execute traffic scenarios.

# Requirements
To execute this project ensure that you fulfill the following requirements: 
1. smlk base project in the same directory as this project. E.g. 
    ```
    workspace
    └───smlk
    └───smlk4sumo
    ```
    You can clone the smlk base project via 
    ```git clone https://bitbucket.org/jgreenyer/smlk.git```

1. Install SUMO 
    1. Add system environment variable SUMO_HOME pointing to the installation directory
    1. Add SUMO_HOME to PATH-Variable 
